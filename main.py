from flask import Flask

app = Flask(__name__)


@app.route('/hello/<name>')
def index(name):
    return "Hello World, " + name +"!"


@app.route('/hw')
def display():
    return "Hello World"


if __name__ == "__main__":
    app.run()
